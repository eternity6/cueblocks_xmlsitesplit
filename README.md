# README #

Modulo necessario a Magento 1.9 Per suddividere la propria sitemap in più sitemap tematiche. Il modulo genera una sitemap madre contenente i
link a tutte le altre sitemap. Poi genera una sitemap per le pagine CMS una per le categorie di prodotti e una per i prodotti comprensiva di 
immagini

### Come funziona il modulo? ###

* Il modulo va configurato dal pannello amministrativo di Magento
* Nei grossi progetti come eurofides è opportuna la generazione della mappa via crontab 
* Il comando magerun per generare subito la mappa è:
* ./n98-magerun sys:cron:run sitemapEnhancedPlus_generate

### Soluzioni adottate per rendere il modulo compatibile con le esigenze del cliente ###

* Le immagini che vengono inserite nella mappa prodotti sono di cache e corrispondono a quelle presenti nella pagina di dettaglio prodotto
* Il file da modificare nel caso in cui il mapping della url delle immagini di cache cambino è: 
* app/code/community/CueBlocks/SitemapEnhancedPlus/Model/Processor/Catalog/Product/Image.php


